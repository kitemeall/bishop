#include <iostream>
#include <fstream>
const int SIZE = 8; // ������.
const int BISHOP = 8;
int board[SIZE][SIZE];
int results_count = 0; // ���������� �������.

// ������� showBoard() - ���������� �����.
void showBoard( std::ofstream& out)
{
	

	for (int a = 0; a < SIZE; ++a)
	{
		for (int b = 0; b < SIZE; ++b)
		{
			out << ((board[a][b]) ? "B " : ". ");
		}
		out << '\n';
	}
}

// ������� tryQueen() - ��������� ��� �� ��� ������������� ������,
// �� ���������, ����������.
bool tryQueen(int a, int b)
{
	

	for (int i = 1; i <= a && b - i >= 0; ++i)
	{
		if (board[a - i][b - i])
		{
			return false;
		}
	}

	for (int i = 1; i <= a && b + i < SIZE; i++)
	{
		if (board[a - i][b + i])
		{
			return false;
		}
	}

	return true;
}

// ������� setQueen() - ������� ����� ���������� �������.
void setQueen(int a, int b, int c, std::ofstream& out) // a - ����� ��������� ������ � ������� ����� ��������� ���������� �����.
{
	if (c == BISHOP)
	{
		//showBoard(out);
		//out << "Result #" << ++results_count << "\n\n";
		++results_count;
		return; // �����������.
	}

	for (int j = b + 1; j < SIZE; j++)
	{
		// ����� ���������, ��� ���� �������� � board[a][i] ����� (�������),
		// �� �� ����� ������������ � ���� ������, ������� � ����������.
		if (tryQueen(a, j))
		{
			board[a][j] = 1;
			setQueen(a, j, c + 1, out);
			board[a][j] = 0;
		}
	}

	for (int i = a + 1; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
			if (tryQueen(i, j))
			{
				board[i][j] = 1;
				setQueen(i, j, c + 1, out);
				board[i][j] = 0;
			}
	}

	return; // �����������.
}

int main()
{
	std::ofstream out("output.txt");
	setQueen(0, -1, 0, out);
	if (!results_count)
		out << "No solutions!";
	else
		out << results_count;
	out.close();
	return 0;
}